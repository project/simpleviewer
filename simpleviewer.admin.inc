<?php


/**
 * @file Section admin for simpleviewer
 */
function simpleviewer_settings_form() {
	$form = array();
	
	$form['simpleviewer_textcolor'] = array(
		'#type' => 'colorpicker_textfield',
		'#title' => t('Text color'),
		'#default_value' => variable_get('simpleviewer_textcolor', '#FFFFFF'),
	);
	$form['simpleviewer_framecolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Frame color'),
    '#default_value' => variable_get('simpleviewer_framecolor', '#FFFFFF'),
	);
	$form['simpleviewer_thumbposition'] = array(
		'#type' => 'select',
		'#title' => t('Thumb position'),
		'#options' => array('TOP' => 'Top', 'BOTTOM' => 'Bottom', 'RIGHT' => 'Right', 'LEFT' => 'Left', 'NONE' => 'None'),
		'#default_value' => variable_get('simpleviewer_thumbposition', 'LEFT'),
	);
	$form['simpleviewer_gallerystyle'] = array(
		'#type' => 'select',
		'#title' => t('Gallery style'),
		'#options' => array('MODERN' => 'Modern', 'COMPACT' => 'Compact', 'CLASSIC' => 'Classic'),
		'#default_value' => variable_get('simpleviewer_gallerystyle', 'MODERN'),
	);
	$form['simpleviewer_thumbcolums'] = array(
		'#type' => 'textfield',
		'#size' => 10,
		'#title' => t('Thumb columns'),
		'#default_value' => variable_get('simpleviewer_thumbcolums', 3),
	);
	$form['simpleviewer_thumbrows'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Thumb rows'),
    '#default_value' => variable_get('simpleviewer_thumbrows', 3),
  );
	$form['framewidth'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Frame width'),
    '#default_value' => variable_get('simpleviewer_framewidth', 20),
  );
	$form['maximagewidth'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Thumb columns'),
    '#default_value' => variable_get('simpleviewer_maximagewidth', 640),
  );
	$form['maximageheight'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Max image height'),
    '#default_value' => variable_get('simpleviewer_maximageheight', 640),
  );
	$form['simpleviewer_showopenbutton'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show open button'),
		'#default_value' => variable_get('simpleviewer_showopenbutton', true),
	);
	$form['simpleviewer_showfullscreenbutton'] = array(
		'#type' => 'checkbox',
		'#title' => t('Full screen button'),
		'#default_value' => variable_get('simpleviewer_showfullscreenbutton', true),
	);

	return system_settings_form($form);
}
