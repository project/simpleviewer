Add a formatter for imagefield fields. The formatter to create a gallery simpleviewer flash plugin.


Maintanier: jacintocapote@gmail.com

How to use it:
1. Download simpleviewer from http://simpleviewer.net/simpleviewer/
2. Extract simpleviewer.swf and place it inside the modules directory, in a directory called simpleviewer, for example:
    /sites/all/modules/simpleviewer/simpleviewer/simpleviewer.swf
3. Setup an Image field with multiple values and go to the display tab, choose 'Gallery simpleviewer' as your field formatter.
